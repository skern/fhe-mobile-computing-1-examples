package de.fhe.ai.vpe;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter
{

    public ViewPagerAdapter( FragmentManager fm ) { super( fm ); }

    @Override
    public Fragment getItem( int pos )
    {
        return SectionFragment.newInstance( pos + 1 );
    }

    @Override
    public int getCount() { return 17; }

    @Override
    public CharSequence getPageTitle( int pos )
    {
        return "SECTION " + ( pos + 1 );
    }
}




