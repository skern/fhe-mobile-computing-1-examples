package de.fhe.ai.vpe;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SectionFragment extends Fragment
{
    private static final String ARG_SECTION_NUMBER = "section_number";

    public static SectionFragment newInstance(int sectionNumber)
    {
        SectionFragment fragment = new SectionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SectionFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_view_pager, container, false);
        TextView textView = (TextView)rootView.findViewById( R.id.section_label );
        textView.setText( Integer.toString( getArguments().getInt( ARG_SECTION_NUMBER ) ) );
        return rootView;
    }
}




