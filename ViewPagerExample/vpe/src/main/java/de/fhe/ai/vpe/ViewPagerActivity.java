package de.fhe.ai.vpe;

import android.app.Activity;
import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

public class ViewPagerActivity extends Activity implements ActionBar.TabListener {

    ViewPagerAdapter pagerAdapter;
    ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_view_pager );

        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode( ActionBar.NAVIGATION_MODE_TABS );

        this.pagerAdapter = new ViewPagerAdapter( getFragmentManager() );

        this.pager = (ViewPager)findViewById( R.id.pager );
        this.pager.setAdapter( this.pagerAdapter );

        this.pager.setOnPageChangeListener( new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected( int position )
            {
                actionBar.setSelectedNavigationItem( position );
            }
        } );

        for( int i = 0; i < this.pagerAdapter.getCount(); i++ )
        {
            ActionBar.Tab tab = actionBar.newTab();
            tab.setText( this.pagerAdapter.getPageTitle( i ) );
            tab.setTabListener( this );

            actionBar.addTab( tab );
        }
    }

    /*
        TabListener Interface Implementation
     */

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        this.pager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}

}
