package de.fhe.ai.servertemplate.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ValueObject
{
    private String name;

    public ValueObject() {}

    public ValueObject( String name )
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ValueObject{" +
                "name='" + name + '\'' +
                '}';
    }
}
