package de.fhe.ai.servertemplate.core;

import de.fhe.ai.servertemplate.resources.Resource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath( "api" )
public class RestApplication extends Application
{
    public RestApplication( ) {}

    @Override
    public Set<Class<?>> getClasses( )
    {
        final Set<Class<?>> returnValue = new HashSet<Class<?>>( );
        returnValue.add( Resource.class );
        return returnValue;
    }

}
