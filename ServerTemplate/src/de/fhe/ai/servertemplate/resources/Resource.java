package de.fhe.ai.servertemplate.resources;

import de.fhe.ai.servertemplate.model.ValueObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path( "/data" )
public class Resource
{
    @GET
    @Produces( { MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML } )
    public ValueObject getObject()
    {
        return new ValueObject( "hello world" );
    }

    @GET
    @Produces( MediaType.TEXT_PLAIN )
    public String getObjectAsString()
    {
        return new ValueObject( "hello world" ).toString();
    }

}