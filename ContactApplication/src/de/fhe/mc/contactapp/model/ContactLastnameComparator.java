package de.fhe.mc.contactapp.model;

import java.util.Comparator;

public class ContactLastnameComparator implements Comparator<Contact>
{
    @Override
    public int compare(Contact c1, Contact c2)
    {
        int lastnameCompareResult = c1.getLastname().compareTo( c2.getLastname() );

        if( lastnameCompareResult != 0 )
            return lastnameCompareResult;
        else
            return c1.getFirstname().compareTo( c2.getFirstname() );
    }
}
