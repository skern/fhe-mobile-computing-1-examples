package de.fhe.mc.contactapp.model;

import java.io.Serializable;
import java.util.List;

public class Contact implements Serializable
{
    private static final String LOG_TAG = "Contact_Class";

    private String lastname;
    private String firstname;
    private String email;
    private String phone;

    public Contact(String lastname, String firstname, String phone, String email )
    {
        this.email = email;
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
    }

    /*
        Getter & Setter
     */

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Override
    public String toString()
    {
        return this.firstname + " " + this.lastname;
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o ) return true;
        if ( o == null || ((Object) this).getClass() != o.getClass() ) return false;

        Contact contact = (Contact) o;

        if ( email != null ? !email.equals( contact.email ) : contact.email != null )
            return false;

        if ( !firstname.equals( contact.firstname ) )
            return false;

        if ( !lastname.equals( contact.lastname ) )
            return false;

        if ( phone != null ? !phone.equals( contact.phone ) : contact.phone != null )
            return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = lastname.hashCode();
        result = 31 * result + firstname.hashCode();
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        return result;
    }
}
