package de.fhe.mc.contactapp.control;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import de.fhe.mc.contactapp.model.Contact;
import de.fhe.mc.contactapp.model.ContactList;
import de.fhe.mc.contactapp.util.StorageWrapper;
import de.fhe.mc.contactapp.view.EditContactActivity;

public class ContactAppController extends Application
{
    private static final String LOG_TAG = "AppController";

    private static Context context;
    private ContactList contacts;

    public void onCreate()
    {
        super.onCreate();
        context = getApplicationContext();
        this.contacts = StorageWrapper.loadContacts();

        Log.i( LOG_TAG, "Ctrl Contacts after load: " + this.contacts.getContactList() );

    }

    /*
        Model related Methods
        Mainly just delegate methods that map to the corresponding
        ContactList methods
     */

    public Contact getContact( int contactID )
    {
        return this.contacts.getContactList().get( contactID );
    }

    public void saveNewContact( String lastname, String firstname, String phone, String email )
    {
        this.contacts.addContact( new Contact( lastname, firstname, phone, email ) );

        this.saveContacts();
    }

    public void updateContact( int contactID, String lastname, String firstname, String phone, String mail )
    {
        this.contacts.updateContact( contactID, lastname, firstname, phone, mail );

        this.saveContacts();
    }

    public void removeAllContacts()
    {
        this.contacts.removeAllContacts();

        this.saveContacts();
    }

    /*
        Public ContactList Modifier
     */

    public void sortContactsByLastname()
    {
        this.contacts.sortByLastname();
    }

    /*
        Navigation/App related Methods
     */
    public void createNewContact()
    {
        this.editContact( EditContactActivity.NEW_CONTACT );
    }

    public void editContact( int contactID )
    {
        Intent i = new Intent( getAppContext(), EditContactActivity.class );

        i.putExtra( EditContactActivity.KEY_CONTACT_ID, contactID );
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        getAppContext().startActivity(i);
    }


    /*
        Global Helper
     */

    // Access to Context
    public static Context getAppContext()
    {
        return context;
    }

    // Access to Contact List
    public ContactList getContacts()
    {
        return this.contacts;
    }


    private void saveContacts()
    {
        StorageWrapper.saveContacts( this.contacts );
    }
}
