package de.fhe.ai.activityinteraction.control;

import android.app.Application;

import de.fhe.ai.activityinteraction.model.Person;

public class AIApplication extends Application
{
    private Person person = new Person( "Max", "Mustermann" );

    public Person getPerson()
    {
        return person;
    }
}
