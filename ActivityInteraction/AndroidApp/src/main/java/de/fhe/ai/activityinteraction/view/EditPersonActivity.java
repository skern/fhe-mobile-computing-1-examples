package de.fhe.ai.activityinteraction.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import de.fhe.ai.activityinteraction.R;
import de.fhe.ai.activityinteraction.control.AIApplication;
import de.fhe.ai.activityinteraction.model.Person;

public class EditPersonActivity extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_person );

        // Init view
        Person p = ((AIApplication)getApplication()).getPerson();
        EditText firstnameInput = (EditText)findViewById( R.id.input_firstname );
        EditText lastnameInput = (EditText)findViewById( R.id.input_lastname );

        firstnameInput.setText( p.getFirstname() );
        lastnameInput.setText( p.getLastname() );
    }

    /*
        Handle Ok Button
     */
    public void okClicked(View view)
    {
        Person p = ((AIApplication)getApplication()).getPerson();

        // Handle Firstname
        EditText firstnameInput = (EditText)findViewById( R.id.input_firstname );
        String newFirstname = firstnameInput.getText().toString();
        if( newFirstname != null && newFirstname.trim().length() > 0 )
            p.setFirstname( newFirstname );

        // Handle Lastname
        EditText lastnameInput = (EditText)findViewById( R.id.input_lastname );
        String newLastname = lastnameInput.getText().toString();
        if( newLastname != null && newLastname.trim().length() > 0 )
            p.setLastname( newLastname );

        finish();
    }

    /*
        Handle Cancel Button
     */
    public void cancelClicked(View view)
    {
        finish();
    }
}
