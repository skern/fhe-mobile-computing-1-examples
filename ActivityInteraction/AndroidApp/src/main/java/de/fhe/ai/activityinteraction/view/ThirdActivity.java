package de.fhe.ai.activityinteraction.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import de.fhe.ai.activityinteraction.R;

public class ThirdActivity extends Activity
{
    public static final String RESULT_KEY = "input_value";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_third );

    }

    public void saveInput(View view)
    {
        EditText et = (EditText)findViewById(R.id.text_input );
        String inputText = et.getText().toString();

        Intent resultIntent = new Intent();
        resultIntent.putExtra( RESULT_KEY, inputText );

        setResult( RESULT_OK, resultIntent );

        finish();
    }
}
