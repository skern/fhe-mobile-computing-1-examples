package de.fhe.ai.activityinteraction.model;

import java.util.ArrayList;
import java.util.List;

public class Person
{
    /*
        Interface for Observers
     */
    public interface PersonObserver
    {
        public static final int FIRSTNAME_CHANGED = 1;
        public static final int LASTNAME_CHANGED = 2;
        public void personChanged( int changeType );
    }

    private String firstname;
    private String lastname;

    // Internal list of observers
    private List<PersonObserver> observers = new ArrayList<PersonObserver>();

    public Person(String firstname, String lastname)
    {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        if( !firstname.equals( this.firstname ) )
        {
            this.firstname = firstname;
            notifyObservers( PersonObserver.FIRSTNAME_CHANGED );
        }
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        if( !lastname.equals( this.lastname ) )
        {
            this.lastname = lastname;
            notifyObservers( PersonObserver.LASTNAME_CHANGED );
        }
    }

    /*
        Observer Handling
     */
    public void register( PersonObserver newObserver )
    {
        this.observers.add( newObserver );
    }

    public void deregister( PersonObserver observer )
    {
        this.observers.remove( observer );
    }

    public void notifyObservers( int changeType )
    {
        for( PersonObserver o : observers )
        {
            o.personChanged( changeType );
        }
    }

}
