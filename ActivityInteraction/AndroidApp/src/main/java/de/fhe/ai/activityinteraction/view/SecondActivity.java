package de.fhe.ai.activityinteraction.view;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import de.fhe.ai.activityinteraction.R;

public class SecondActivity extends Activity
{
    public static final String BUNDLE_KEY = "key";
    private static final String DEFAULT_VALUE = "No Value received";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_second );

        TextView tv = (TextView)findViewById( R.id.data_view );

        Bundle bundle = getIntent().getExtras();

        if( bundle != null )
        {
            String receivedValue = bundle.getString( BUNDLE_KEY );

            if( receivedValue != null )
            {
                tv.setText( receivedValue );
            }
            else
            {
                tv.setText( DEFAULT_VALUE );
            }
        }
    }
}
