package de.fhe.ai.activityinteraction.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import de.fhe.ai.activityinteraction.R;
import de.fhe.ai.activityinteraction.control.AIApplication;
import de.fhe.ai.activityinteraction.model.Person;

public class MainActivity extends Activity implements Person.PersonObserver
{
    private static final int THIRD_ACTIVITY_RESULT_ID = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
            Add ourself as observer to the person object
            See Example 3 below as well as Person class
         */
        Person p = ((AIApplication)getApplication()).getPerson();
        p.register( this );
    }

    /*
        Example 1 - Start second activity
     */

    public void openActivityClicked( View view )
    {
        Intent i = new Intent( getApplicationContext(), SecondActivity.class );
        i.putExtra( SecondActivity.BUNDLE_KEY, "Hello World" );
        startActivity( i );
    }

    /*
        Example 2 - Sub-Activity and Results
     */
    public void openActivityForResultClicked( View view )
    {
        Intent i = new Intent( getApplicationContext(), ThirdActivity.class );
        startActivityForResult( i, THIRD_ACTIVITY_RESULT_ID );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if( requestCode == THIRD_ACTIVITY_RESULT_ID )
        {
            if( resultCode == RESULT_OK )
            {
                if( data.getExtras() != null )
                {
                    String value = data.getExtras().getString( ThirdActivity.RESULT_KEY );

                    if( value != null )
                    {
                        this.appendLogMsg(value);
                    }
                }
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /*
        Example 3 - Application Class / MVC
     */

    public void editPersonClicked(View view)
    {
        Intent i = new Intent( getApplicationContext(), EditPersonActivity.class );
        startActivity( i );
    }

    @Override
    public void personChanged( int changeType )
    {
        Person p = ((AIApplication)getApplication()).getPerson();

        switch (changeType )
        {
            case Person.PersonObserver.FIRSTNAME_CHANGED:
            {
                this.appendLogMsg( "New Firstname:\t " + p.getFirstname() );
                break;
            }
            case Person.PersonObserver.LASTNAME_CHANGED:
            {
                this.appendLogMsg( "New Lastname:\t " + p.getLastname() );
                break;
            }
        }
    }

    /*
        Internal Helper method to write to the logging text view
     */
    private void appendLogMsg( String msg )
    {
        // Get a reference to out logging text view
        TextView logView = (TextView)findViewById( R.id.log_view );

        // Append message
        logView.setText( msg + "\n" + logView.getText() );
    }

}
