package de.fhe.ai.androidmapsv2.demo.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.google.android.gms.maps.model.Tile;
import com.google.android.gms.maps.model.TileProvider;
import de.fhe.ai.androidmapsv2.R;

import java.io.ByteArrayOutputStream;


public class MapTileAdapter implements TileProvider
{
    private Bitmap orangeBitmap;
    private Bitmap blueBitmap;

    private Tile orangeTile;
    private Tile blueTile;

    public MapTileAdapter( Context c )
    {
        this.orangeBitmap = BitmapFactory.decodeResource( c.getResources(), R.drawable.orange);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        this.orangeBitmap.compress( Bitmap.CompressFormat.PNG, 100, stream);

        this.orangeTile = new Tile( this.orangeBitmap.getWidth(),
                this.orangeBitmap.getHeight(), stream.toByteArray() );

        this.blueBitmap = BitmapFactory.decodeResource( c.getResources(), R.drawable.blue);

        ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
        this.blueBitmap.compress( Bitmap.CompressFormat.PNG, 100, stream2);

        this.blueTile = new Tile( this.blueBitmap.getWidth(),
                this.blueBitmap.getHeight(), stream2.toByteArray() );
    }

    @Override
    public Tile getTile( int x, int y, int zoom )
    {
        if( zoom < 12 ) return NO_TILE;

        if( y % 2 == 0 )
        {
            if( x % 2 != 0 )
                return this.orangeTile;
            else
                return this.blueTile;
        }
        else
        {
            if( x % 2 == 0 )
                return this.orangeTile;
            else
                return this.blueTile;
        }
    }
}
