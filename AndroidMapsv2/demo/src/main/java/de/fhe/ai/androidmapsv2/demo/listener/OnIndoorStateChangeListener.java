package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.IndoorBuilding;

public class OnIndoorStateChangeListener implements GoogleMap.OnIndoorStateChangeListener
{
    private static final String LOG_TAG = "Indoor State Change";

    @Override
    public void onIndoorBuildingFocused()
    {
        Log.i( LOG_TAG, "A Building was focused" );
    }

    @Override
    public void onIndoorLevelActivated( IndoorBuilding indoorBuilding )
    {
        Log.i( LOG_TAG, "Building level activated: " + indoorBuilding.getActiveLevelIndex() );
    }
}



