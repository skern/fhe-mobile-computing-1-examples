package de.fhe.ai.androidmapsv2.demo.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import de.fhe.ai.androidmapsv2.R;

public class MapInfoWindowAdapter implements GoogleMap.InfoWindowAdapter
{
    public enum InfoWindowMode { CUSTOM_WINDOW, CUSTOM_CONTENT, DEFAULT; }

    private InfoWindowMode mode;
    private Context context;

    public MapInfoWindowAdapter( Context context, InfoWindowMode mode )
    {
        this.context = context;
        this.mode = mode;
    }

    @Override
    public View getInfoWindow( Marker marker )
    {
        if( this.mode == InfoWindowMode.CUSTOM_WINDOW )
            return this.createIconView();

        return null;
    }

    @Override
    public View getInfoContents( Marker marker )
    {
        if( this.mode == InfoWindowMode.CUSTOM_CONTENT )
            return this.createIconView();

        return null;
    }

    private View createIconView()
    {
        ImageView iconView = new ImageView( this.context );
        iconView.setImageResource( R.drawable.ic_launcher );

        return iconView;
    }
}
