package de.fhe.ai.androidmapsv2.demo;

import android.app.Activity;
import android.os.Bundle;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import de.fhe.ai.androidmapsv2.R;

public class StreetViewActivity extends Activity
{
    public static final String EXTRA_LATLONG = "latlong";

    StreetViewPanorama streetViewPanorama;

    @Override
    protected void onCreate( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_streetview );
    }


    @Override
    protected void onResume()
    {
        super.onResume();

        setUpStreetViewPanoramaIfNeeded();
    }

    private void setUpStreetViewPanoramaIfNeeded()
    {
        if (streetViewPanorama == null)
        {
            streetViewPanorama = ((StreetViewPanoramaFragment)
                    getFragmentManager().findFragmentById(R.id.streetviewpanorama))
                    .getStreetViewPanorama();

            if (streetViewPanorama != null)
            {
                Bundle extras = getIntent().getExtras();
                if( extras != null )
                {
                    LatLng pos = extras.getParcelable( EXTRA_LATLONG );

                    if ( pos != null )
                    {
                        streetViewPanorama.setPosition( pos );

                        return;
                    }
                }

                streetViewPanorama.setPosition( new LatLng( 37.765927, -122.449972 ));
            }
        }
    }
}



