package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class MarkerDragListener implements GoogleMap.OnMarkerDragListener
{
    private static final String LOG_TAG = "Marker Drag";

    @Override
    public void onMarkerDragStart( Marker marker )
    {
        Log.i( LOG_TAG, marker.getTitle() + " - Started - Position: " + marker.getPosition() );
    }

    @Override
    public void onMarkerDrag( Marker marker )
    {
        Log.i( LOG_TAG, marker.getTitle() + " - Dragging - Position: " + marker.getPosition() );
    }

    @Override
    public void onMarkerDragEnd( Marker marker )
    {
        Log.i( LOG_TAG, marker.getTitle() + " - Ended - Position: " + marker.getPosition() );
    }
}