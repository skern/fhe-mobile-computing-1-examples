package de.fhe.ai.androidmapsv2.demo.listener;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import de.fhe.ai.androidmapsv2.demo.StreetViewActivity;

public class InfoWindowClickListener implements GoogleMap.OnInfoWindowClickListener
{
    private static final String LOG_TAG = "Marker Info Window Click";

    private Context appContext;

    public InfoWindowClickListener( Context appContext )
    {
        this.appContext = appContext;
    }

    @Override
    public void onInfoWindowClick( Marker marker )
    {
        Log.i( LOG_TAG, marker.getTitle() + " Info Window Clicked" );

        Intent i = new Intent( this.appContext, StreetViewActivity.class );
        i.putExtra( StreetViewActivity.EXTRA_LATLONG, marker.getPosition() );

        appContext.startActivity( i );

    }
}