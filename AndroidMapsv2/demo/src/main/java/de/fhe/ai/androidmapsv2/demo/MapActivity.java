package de.fhe.ai.androidmapsv2.demo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.*;

import de.fhe.ai.androidmapsv2.R;
import de.fhe.ai.androidmapsv2.demo.adapter.MapInfoWindowAdapter;
import de.fhe.ai.androidmapsv2.demo.adapter.MapTileAdapter;
import de.fhe.ai.androidmapsv2.demo.listener.*;

public class MapActivity extends Activity
{
    // Might be null if Google Play services APK is not available.
    private GoogleMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_maps);

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded()
    {
        // Do a null check to confirm that we have not already instantiated the map.
        if ( this.map == null)
        {
            // Try to obtain the map from the FragmentManager.
            this.map = ((MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map)).getMap();

            // Check if we were successful in obtaining the map.
            if ( this.map != null)
            {
                setUpMap();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu )
    {
        MenuInflater inflater = new MenuInflater( this );
        inflater.inflate( R.menu.map_menu, menu );

        return true;
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item )
    {
        switch ( item.getItemId() )
        {

        //  Handle Map Type Option Menu Items
            case R.id.map_type_normal:
                this.map.setMapType( GoogleMap.MAP_TYPE_NORMAL );
                return true;
            case R.id.map_type_satellite:
                this.map.setMapType( GoogleMap.MAP_TYPE_SATELLITE );
                return true;
            case R.id.map_type_hybrid:
                this.map.setMapType( GoogleMap.MAP_TYPE_HYBRID );
                return true;
            case R.id.map_type_terrain:
                this.map.setMapType( GoogleMap.MAP_TYPE_TERRAIN );
                return true;
            case R.id.map_type_none:
                this.map.setMapType( GoogleMap.MAP_TYPE_NONE );
                return true;

        //  Handle Zoom Level Option Menu Items
            case R.id.map_zoom_01:
                this.map.animateCamera( CameraUpdateFactory.zoomTo( 1.0f ) );
                return true;
            case R.id.map_zoom_06:
                this.map.animateCamera( CameraUpdateFactory.zoomTo( 6.0f ) );
                return true;
            case R.id.map_zoom_12:
                this.map.animateCamera( CameraUpdateFactory.zoomTo( 12.0f ) );
                return true;

        //  Handle Camera Movement
            case R.id.map_move:
                // Berlin
                this.map.animateCamera(
                        CameraUpdateFactory.newLatLng(
                                new LatLng( 52.529329, 13.406067 )
                        )
                );

                return true;

            case R.id.map_move_zoom:
                // Hamburg
                this.map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                                new LatLng( 53.545892, 10.022278 ),
                                8.0f
                        )
                );

                return true;

            case R.id.map_move_zoom_tilt:
                // München
                CameraPosition newPosition = new CameraPosition(
                        new LatLng( 48.135814,11.581306 ),
                        10.0f,
                        45.0f,
                        0.0f);

                this.map.animateCamera(
                        CameraUpdateFactory.newCameraPosition( newPosition ) );

                return true;

            case R.id.map_move_zoom_tilt_bearing:
                // Frankfurt
                CameraPosition newBearingPosition =
                        new CameraPosition( new LatLng( 50.110196,8.685379 ),
                                7.0f,
                                60.0f,
                                60.0f);

                this.map.animateCamera(
                        CameraUpdateFactory.newCameraPosition( newBearingPosition ) );

                return true;
            case R.id.map_rotate_left:
                CameraPosition oldPosition =
                        this.map.getCameraPosition();

                CameraPosition leftRotation =
                        new CameraPosition( oldPosition.target, oldPosition.zoom,
                                oldPosition.tilt, oldPosition.bearing - 60.0f );

                this.map.animateCamera(
                        CameraUpdateFactory.newCameraPosition( leftRotation ) );

                return true;
            case R.id.map_rotate_right:
                CameraPosition oldPosition02 =
                        this.map.getCameraPosition();

                CameraPosition rightRotation =
                        new CameraPosition( oldPosition02.target, oldPosition02.zoom,
                                oldPosition02.tilt, oldPosition02.bearing + 60.0f );

                this.map.animateCamera(
                        CameraUpdateFactory.newCameraPosition( rightRotation ) );

                return true;

            case R.id.map_focus:

                LatLngBounds bounds =
                        new LatLngBounds( new LatLng(-44, 113), new LatLng(-10, 154) );

                this.map.animateCamera(
                        CameraUpdateFactory.newLatLngBounds( bounds, 20 ) );

                return true;

            case R.id.map_move_up:
                this.map.animateCamera( CameraUpdateFactory.scrollBy( 0, 150.0f ) );
                return true;
            case R.id.map_move_down:
                this.map.animateCamera( CameraUpdateFactory.scrollBy( 0, -150.0f ) );
                return true;
            case R.id.map_move_left:
                this.map.animateCamera( CameraUpdateFactory.scrollBy( 150.0f, 0 ) );
                return true;
            case R.id.map_move_right:
                this.map.animateCamera( CameraUpdateFactory.scrollBy( -150.0f, 0 ) );
                return true;

        // Handle Overlay Options
            case R.id.map_add_polyline:


                PolylineOptions lineDef = new PolylineOptions()
                        .add( new LatLng( 50.0, 10.0 ) )
                        .add( new LatLng( 51.0, 10.0 ) )
                        .add( new LatLng( 51.0, 11.0 ) )
                        .add( new LatLng( 50.0, 11.0 ) );

                Polyline polyline = this.map.addPolyline( lineDef );
                polyline.setColor( Color.RED );

                LatLngBounds lineBounds =
                        new LatLngBounds( new LatLng(50, 10), new LatLng(51, 11) );
                this.map.animateCamera(
                        CameraUpdateFactory.newLatLngBounds( lineBounds, 100 ) );




                return true;
            case R.id.map_add_polygon:

                // Instantiates a new Polygon object and adds points
                // to define a rectangle
                PolygonOptions polygonDef = new PolygonOptions()
                        .add(   new LatLng(52.0, 12.0),
                                new LatLng(52.5, 12.5),
                                new LatLng(52.0, 13.0),
                                new LatLng(53.0, 12.5))
                        .fillColor( Color.CYAN );

                // Get back the mutable Polygon
                Polygon polygon = this.map.addPolygon( polygonDef );

                LatLngBounds polygonBounds =
                        new LatLngBounds( new LatLng(52, 12), new LatLng(53, 13) );
                this.map.animateCamera(
                        CameraUpdateFactory.newLatLngBounds( polygonBounds, 100 ) );

                return true;

            case R.id.map_add_circle:

                CircleOptions circleDef = new CircleOptions()
                        .center(new LatLng(48.798009,9.19796))
                        .radius(5000); // In meters

                Circle circle = this.map.addCircle( circleDef );
                circle.setFillColor( Color.BLUE );
                circle.setStrokeColor( Color.DKGRAY );
                circle.setStrokeWidth( 20.0f );

                this.map.animateCamera(
                        CameraUpdateFactory.newLatLng(
                                new LatLng( 48.798009,9.19796 ) ) );

                return true;

            case R.id.map_clear_all_overlays:
                this.map.clear();
                return true;

            case R.id.map_add_tile_overlay:
                // Add Map Tile Provider
                TileOverlay tileOverlay = this.map.addTileOverlay(
                        new TileOverlayOptions().tileProvider(
                                new MapTileAdapter( this )));

                return true;

            default:
                return super.onOptionsItemSelected( item );
        }
    }



    private void setUpMap()
    {
        // Configure Listener
        this.map.setOnMapClickListener( new MapClickListener() );
        this.map.setOnMapLongClickListener( new MapLongClickListener( this.map ) );
        this.map.setOnMarkerClickListener( new MarkerClickListener() );
        this.map.setOnMarkerDragListener( new MarkerDragListener() );
        this.map.setOnInfoWindowClickListener( new InfoWindowClickListener( this ) );
        this.map.setOnMyLocationButtonClickListener( new MyLocationButtonListener() );
        this.map.setOnCameraChangeListener( new CameraChangeListener() );
        this.map.setOnIndoorStateChangeListener( new OnIndoorStateChangeListener() );

        // Configure Map Properties
        this.map.setIndoorEnabled( true );
        this.map.setBuildingsEnabled( true );
        this.map.setMyLocationEnabled( true );
        this.map.setTrafficEnabled( false );

        // Configure Marker Info Window Adapter
        MapInfoWindowAdapter mapAdapter =
                new MapInfoWindowAdapter( this, MapInfoWindowAdapter.InfoWindowMode.DEFAULT );
        this.map.setInfoWindowAdapter( mapAdapter );


        // Add several marker
        Marker eapMarker = map.addMarker( new MarkerOptions()
                    .position( new LatLng( 50.928672, 11.581816 ) )
                    .title( "Ernst-Abbe-Platz" )
                    .snippet( "Jena" )
                    .flat( true )
                    .rotation( 45.0f ));

        // Show the info window of the first marker
        eapMarker.showInfoWindow();

        this.map.addMarker( new MarkerOptions()
                .position( new LatLng( 50.991389, 11.326432 ) )
                .title( "Hauptbahnhof" )
                .snippet( "Weimar" )
                .icon( BitmapDescriptorFactory.fromResource(
                        R.drawable.ic_launcher ) ));

        this.map.addMarker( new MarkerOptions()
                .position( new LatLng( 50.985197,11.042445 ) )
                .title( "FH Campus" )
                .snippet( "Erfurt" )
                .icon( BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_BLUE) )
                .alpha( 0.75f ) );

        // Add FH Erfurt Campus Map as Ground Overlay
        this.map.addGroundOverlay( new GroundOverlayOptions()
                .image( BitmapDescriptorFactory.fromResource( R.drawable.map_campus ) )
                .position( new LatLng( 50.984876, 11.042596 ), 263f )
                .transparency( 0.5f )
                .bearing( 54f ) );

        // Add a draggable marker
        this.map.addMarker( new MarkerOptions()
                .position( new LatLng( 51.111147, 11.276093 ) )
                .title( "Draggable Marker" )
                .snippet( "Long-Press to Drag" )
                .draggable( true ) );

    }
}
