package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class MarkerClickListener implements GoogleMap.OnMarkerClickListener
{
    private static final String LOG_TAG = "Marker Click";

    @Override
    public boolean onMarkerClick( Marker marker )
    {
        Log.i( LOG_TAG, marker.getTitle() + " clicked" );

        // Returning false to allow for further processing of the click event
        // e.g. show the info window
        // If we would return true, we would "consume" the event and no
        // further processing would take place, e.g. no info window is shown
        return false;
    }
}


