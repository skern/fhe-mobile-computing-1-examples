package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class MapClickListener implements GoogleMap.OnMapClickListener
{
    private static final String LOG_TAG = "Map Click";

    @Override
    public void onMapClick( LatLng latLng )
    {
        Log.i( LOG_TAG, "Map Clicked on " + latLng );
    }
}



