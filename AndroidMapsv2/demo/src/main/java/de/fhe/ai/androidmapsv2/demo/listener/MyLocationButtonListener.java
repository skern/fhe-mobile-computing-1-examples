package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;

public class MyLocationButtonListener implements GoogleMap.OnMyLocationButtonClickListener
{
    private static final String LOG_TAG = "Location Button Click";

    @Override
    public boolean onMyLocationButtonClick()
    {
        Log.i( LOG_TAG, "My Location Button clicked" );

        // Return false to allow for further processing of the location button
        return false;
    }
}



