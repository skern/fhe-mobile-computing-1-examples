package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;

public class CameraChangeListener implements GoogleMap.OnCameraChangeListener
{
    private static final String LOG_TAG = "Camera Listener";

    @Override
    public void onCameraChange( CameraPosition cameraPosition )
    {
        Log.i( LOG_TAG, "Camera changed to " + cameraPosition);
    }
}




