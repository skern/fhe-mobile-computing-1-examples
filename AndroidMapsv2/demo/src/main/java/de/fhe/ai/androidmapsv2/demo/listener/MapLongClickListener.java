package de.fhe.ai.androidmapsv2.demo.listener;

import android.util.Log;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DecimalFormat;

public class MapLongClickListener implements GoogleMap.OnMapLongClickListener
{
    private static final String LOG_TAG = "Map Long Click Listener";

    private GoogleMap map;
    private DecimalFormat format = new DecimalFormat( "#.####" );

    public MapLongClickListener( GoogleMap map )
    {
        this.map = map;
    }

    @Override
    public void onMapLongClick( LatLng latLng )
    {
        Log.i( LOG_TAG, "Long Click at " + latLng );

        this.map.addMarker( new MarkerOptions()
                .position( latLng )
                .title( format.format( latLng.latitude ) + " / " +
                        format.format( latLng.longitude ) ) );
    }
}



